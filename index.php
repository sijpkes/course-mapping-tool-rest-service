<?php
/**
    This is the REST service for the course mapping tool
*/
require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();

// ... definitions
$app['debug'] = true;

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
		'db.options' => array(
			'driver'    => 'pdo_mysql',
            'host'      => 'localhost',
            'dbname'    => 'course_mapper',
            'user'      => '#######',
            'password'  => '#######',
            'charset'   => 'utf8',
		),
));

/**
    Set our default comms to use JSON
*/
$app->before(function (Request $request) {
	if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
		$data = json_decode($request->getContent(), true);
		$request->request->replace(is_array($data) ? $data : array());
	}
});

/*
 * Courses
 */
 /*
  * Create new course
  */
$app->post('/courses', function(Request $request) use($app) {

	$name = $request->request->get('name');
	$code = $request->request->get('code');
	$duration = (int) $request->request->get('duration');

	$app['db']->insert("courses", array('name' => $name, 'code' => $code, 'duration' => $duration));

	$id =  $app['db']->lastInsertId();
	$row = array("id" => $id, "name" => $name, "code" => $code, "duration" => $duration);

	$location = "/courses/".$id;

	$response = buildResponse(json_encode($row), Response::HTTP_CREATED, $location);
	return $response;
});

$app->get('/courses/{id}', function(Request $request, $id) use($app) {
	$sql = sprintf("SELECT * FROM courses WHERE id = '%d'", $id);;
	$post = $app['db']->fetchAll($sql);

	$response = buildResponse(json_encode($post[0]), Response::HTTP_OK);
	return $response;
});

$app->delete('/courses/{id}', function(Request $request, $id) use($app) {
	$app['db']->delete("courses", array('id' => $id));
	$obj = json_encode(array("id" => $id));

	$response = buildResponse(json_encode($obj), Response::HTTP_OK);

	return $response;
});

$app->put('/courses/{id}', function(Request $request, $id) use($app) {
	$name = $request->request->get('name');
	$code = $request->request->get('code');
	$duration = (int) $request->request->get('duration');

	$app['db']->update("courses", array('name' => $name, 'code' => $code, 'duration' => $duration),  array('id' => $id));

	$obj = json_encode(array("id" => $id, 'name' => $name, 'code' => $code, 'duration' => $duration));
	$response = buildResponse(json_encode($obj), Response::HTTP_OK);

	return $response;
});

$app->get('/courses', function(Request $request) use ($app) {
		$page = $request->get("page");
		$limit = $request->get("limit");

		$offset = ($page - 1) * $limit;

		$sql = sprintf("SELECT * FROM courses LIMIT %d , %d", $offset, $limit);
		$post = $app['db']->fetchAll($sql);

		$response = buildResponse(json_encode($post), Response::HTTP_OK);
		return $response;
});
/*
 * Learning Outcomes
 */
$app->post('/courses/{course_id}/learning_outcomes', function(Request $request, $course_id) use($app) {
		$name = $request->request->get('name');
		$details = $request->request->get('details');
		//$course_id = $request->request->get('course_id');

		if(!is_numeric($course_id)) {
			return buildResponse(json_encode(array("error" => "Bad course id")), Response::HTTP_BAD_REQUEST);
		}

		$app['db']->insert("learning_outcomes", array('name' => $name, 'details' => $details, 'course_id' => $course_id));
		$id =  $app['db']->lastInsertId();

		$row = array("id" => $id, 'name' => $name, 'details' => $details, 'course_id' => $course_id);

		$location = "/courses/$course_id/learning_outcomes/$id";

		$response = buildResponse(json_encode($row), Response::HTTP_CREATED, $location);

		return $response;
});


$app->get('/courses/{course_id}/learning_outcomes', function(Request $request, $course_id) use($app) {
		$page = $request->get("page");
		$limit = $request->get("limit");
		//$course_id = $request->get("course_id");

		if(empty($course_id) || !is_numeric($course_id)) {
			return buildResponse(json_encode(array("error" => "Bad course id")), Response::HTTP_BAD_REQUEST);
		}

		$offset = ($page - 1) * $limit;

		$sql = sprintf("SELECT * FROM learning_outcomes WHERE course_id = '%d' LIMIT %d, %d", $course_id, $offset, $limit);
		$post = $app['db']->fetchAll($sql);
		$response = buildResponse(json_encode($post), Response::HTTP_OK);

		return $response;
});

$app->get('/courses/{course_id}/learning_outcomes/{id}', function(Request $request, $course_id, $id) use($app) {

		if(empty($course_id) || !is_numeric($course_id)) {
			return buildResponse(json_encode(array("error" => "Bad course id")), Response::HTTP_BAD_REQUEST);
		}

		$offset = ($page - 1) * $limit;

		$sql = sprintf("SELECT * FROM learning_outcomes WHERE id = '%d' AND course_id = '%d'", $id, $course_id, $offset, $limit);
		$post = $app['db']->fetchAll($sql);
		$response = buildResponse(json_encode($post[0]), Response::HTTP_OK);

		return $response;
});

$app->put('/courses/{course_id}/learning_outcomes/{id}', function(Request $request, $course_id, $id) use($app) {
	$name = $request->request->get('name');
	$details = $request->request->get('details');

	if(!is_numeric($id)) {
		return buildResponse(json_encode(array("error" => "Bad learning outcome id")), Response::HTTP_BAD_REQUEST);
	}

	$app['db']->update("learning_outcomes", array('name' => $name, 'details' => $details, 'course_id' => $course_id),  array('id' => $id, 'course_id' => $course_id));

	$row = array("id" => $id, 'name' => $name, 'details' => $details, 'course_id' => $course_id);

	$response = buildResponse(json_encode($row), Response::HTTP_OK);

	return $response;
});

$app->delete('/courses/{course_id}/learning_outcomes/{id}', function(Request $request, $course_id, $id) use($app) {

		if(!is_numeric($id)) {
			return buildResponse(json_encode(array("error" => "Bad learning outcome id")), Response::HTTP_BAD_REQUEST);
		}

		$app['db']->delete("learning_outcomes", array('id' => $id, 'course_id' => $course_id));
		$row = array("id" => $id, 'course_id' => $course_id);

		$response = buildResponse(json_encode($row), Response::HTTP_OK);

		return $response;
});

/*
 * Learning Activities
 */

/*
 * Create a new learning activity
 */
$app->post('/courses/{course_id}/learning_activities', function(Request $request, $course_id) use($app) {
		$name = $request->request->get('name');
		$details = $request->request->get('details');
		$learning_outcomes = $request->request->get('learning_outcomes');
		$type =  $request->request->get('type');
		$week = $request->request->get('week');

		if(!empty($learning_outcomes)) {
				if(!is_array($learning_outcomes)) {
					return buildResponse(json_encode(array("error" => "Learning outcomes must be an array.")), Response::HTTP_BAD_REQUEST);
				}

				$learning_outcomes = json_encode($learning_outcomes);
		} else {
			$learning_outcomes = json_encode(array());
		}

		if(!is_numeric($week) || $week < 1 || $week > 255) {
			return buildResponse(json_encode(array("error" => "Week must be an integer between 1 and 255")), Response::HTTP_BAD_REQUEST);
		}

		if(!is_numeric($course_id)) {
			return buildResponse(json_encode(array("error" => "Bad course id")), Response::HTTP_BAD_REQUEST);
		}

		$app['db']->insert("learning_activities", array('name' => $name, 'details' => $details, 'type' => $type, 'course_id' => $course_id, 'learning_outcomes' => $learning_outcomes, 'week' => $week));
		$id =  $app['db']->lastInsertId();

		$row = array("id" => $id, 'name' => $name, 'type' => $type, 'details' => $details, 'course_id' => $course_id, 'learning_outcomes' => $learning_outcomes, 'week' => $week);

		$location = "/courses/$course_id/learning_activities/$id";

		$response = buildResponse(json_encode($row), Response::HTTP_CREATED, $location);

		return $response;
});
/*
 * Get learning activities for a course
 */
$app->get('/courses/{course_id}/learning_activities', function(Request $request, $course_id) use($app) {
	$page = $request->get("page");
	$limit = $request->get("limit");

	if(empty($course_id) || !is_numeric($course_id)) {
		return buildResponse(json_encode(array("error" => "Bad course id - must be numeric")), Response::HTTP_BAD_REQUEST);
	}

	$offset = ($page - 1) * $limit;

	$sql = sprintf("SELECT * FROM learning_activities WHERE course_id = '%d' LIMIT %d, %d", $course_id, $offset, $limit);
	$post = $app['db']->fetchAll($sql);
	$response = buildResponse(json_encode($post), Response::HTTP_OK);

	return $response;
});
/*
 * Get single learning activity
 *
 */
$app->get('/courses/{course_id}/learning_activities/{id}', function(Request $request, $course_id, $id) use($app) {

		if(empty($course_id) || !is_numeric($course_id)) {
			return buildResponse(json_encode(array("error" => "Bad course id")), Response::HTTP_BAD_REQUEST);
		}

		$sql = sprintf("SELECT * FROM learning_activities WHERE id = '%d'", $id);
		$post = $app['db']->fetchAll($sql);
		$response = buildResponse(json_encode($post[0]), Response::HTTP_OK);

		return $response;
});
/*
 * Update single learning activity
 */
$app->put('/courses/{course_id}/learning_activities/{id}',
			function(Request $request, $course_id, $id) use($app) {
			$name = $request->request->get('name');
			$details = $request->request->get('details');
			$type =  $request->request->get('type');
			$learning_outcomes = $request->request->get('learning_outcomes');
			$week =  $request->request->get('week');

			if(!empty($learning_outcomes)) {
				if(!is_array($learning_outcomes)) {

					$learning_outcomes = json_decode($learning_outcomes);
					//return buildResponse(json_encode(array("error" => "Learning outcomes must be an array.")), Response::HTTP_BAD_REQUEST);
				}

				$learning_outcomes = json_encode($learning_outcomes);
			} else {
				$learning_outcomes = json_encode(array());
			}


			if(!is_numeric($week) || $week < 1 || $week > 255) {
				return buildResponse(json_encode(array("error" => "Week must be an integer between 1 and 255")), Response::HTTP_BAD_REQUEST);
			}

			if(!is_numeric($id)) {
				return buildResponse(json_encode(array("error" => "Bad learning activity id")), Response::HTTP_BAD_REQUEST);
			}

			$app['db']->update("learning_activities", array('name' => $name, 'details' => $details, 'type' => $type, 'learning_outcomes' => $learning_outcomes, 'course_id' => $course_id, 'week' => $week),  array('id' => $id));

			$row = array("id" => $id, 'name' => $name, 'details' => $details, 'type' => $type, 'learning_outcomes' => $learning_outcomes, 'course_id' => $course_id, 'week' => $week);

			$response = buildResponse(json_encode($row), Response::HTTP_OK);

			return $response;
});
/*
 * Delete single learning activity
 */
$app->delete('/courses/{course_id}/learning_activities/{id}',
			function(Request $request, $course_id, $id) use($app) {

	if(!is_numeric($id)) {
		return buildResponse(json_encode(array("error" => "Bad learning activity id")), Response::HTTP_BAD_REQUEST);
	}

	$app['db']->delete("learning_activities", array('id' => $id, 'course_id' => $course_id));
	$row = array("id" => $id, 'course_id' => $course_id);

	$response = buildResponse(json_encode($row), Response::HTTP_OK);

	return $response;
});


/*
 * Assessments
 * -----------
 */
 /*
  * Create a new assessment using POST request
  */
$app->post('/courses/{course_id}/assessments', function(Request $request, $course_id) use($app) {
	$name = $request->request->get('name');
	$learning_outcomes = $request->request->get('learning_outcomes');
	$details = $request->request->get('details');
	$type = $request->request->get("type");
	$week = $request->request->get("week");
	$weight = $request->request->get("weight");

	if(!empty($learning_outcomes)) {
		if(!is_array($learning_outcomes)) {
			$learning_outcomes = json_decode($learning_outcomes);
			//return buildResponse(json_encode(array("error" => "Learning outcomes must be an array.")), Response::HTTP_BAD_REQUEST);
		}

		$learning_outcomes = json_encode($learning_outcomes);
	} else {
		$learning_outcomes = json_encode(array());
	}

	if(!empty($week) && $week !== "ongoing" && !is_numeric($week)) {

		if(strpos($week, "_")) {
			$a = explode("_", $week);

			if(!is_numeric($a[0]) || !is_numeric($a[1])) {
				return buildResponse(json_encode(array("error" => "Week must be an integer, underscore separated range e.g. '2_3' or 'ongoing'")), Response::HTTP_BAD_REQUEST);
			}
		}

		return buildResponse(json_encode(array("error" => "Week must be an integer, underscore separated range e.g. '2_3' or 'ongoing'")), Response::HTTP_BAD_REQUEST);
	}

	if(!is_numeric($weight)) {
		return buildResponse(json_encode(array("error" => "Weight must be single decimal place floating number (e.g. 0.1 for 10%)")), Response::HTTP_BAD_REQUEST);
	} else if($weight < 0 || $weight > 1) {
		return buildResponse(json_encode(array("error" => "Weight must be single decimal place floating number (e.g. 0.1 for 10%)")), Response::HTTP_BAD_REQUEST);
	}

	if(!is_numeric($course_id)) {
		return buildResponse(json_encode(array("error" => "Bad course id")), Response::HTTP_BAD_REQUEST);
	}

	$app['db']->insert("assessments", array('name' => $name, 'details' => $details, 'type' => $type, 'week' => $week, 'weight' => "'".(String) floatval($weight)."'", 'course_id' => $course_id, 'learning_outcomes' => $learning_outcomes));
	$id =  $app['db']->lastInsertId();

	$row = array("id" => $id, 'name' => $name, 'details' => $details, 'type' => $type, 'week' => $week, 'weight' => floatval($weight), 'course_id' => $course_id, 'learning_outcomes' => $learning_outcomes);

	$location = "/courses/$course_id/assessments/$id";

	$response = buildResponse(json_encode($row), Response::HTTP_CREATED, $location);

	return $response;
});

/*
 * Get assessments for course
 */
$app->get('/courses/{course_id}/assessments', function(Request $request, $course_id) use($app) {
	$page = $request->get("page");
	$limit = $request->get("limit");

	if(empty($course_id) || !is_numeric($course_id)) {
		return buildResponse(json_encode(array("error" => "Bad course id - must be numeric")), Response::HTTP_BAD_REQUEST);
	}

	$offset = ($page - 1) * $limit;

	$sql = sprintf("SELECT * FROM assessments WHERE course_id = '%d' LIMIT %d, %d", $course_id, $offset, $limit);
	$post = $app['db']->fetchAll($sql);
	$response = buildResponse(json_encode($post), Response::HTTP_OK);

	return $response;
});
/*
 * Get single assessment
 */
$app->get('/courses/{course_id}/assessments/{id}', function(Request $request, $course_id, $id) use($app) {

	if(empty($id) || !is_numeric($id)) {
		return buildResponse(json_encode(array("error" => "Bad assessment id")), Response::HTTP_BAD_REQUEST);
	}

	$sql = sprintf("SELECT * FROM assessments WHERE id = '%d'", $id);
	$post = $app['db']->fetchAll($sql);
	$response = buildResponse(json_encode($post[0]), Response::HTTP_OK);

	return $response;
});
/*
 * Put single assessment
 */
$app->put('/courses/{course_id}/assessments/{id}',
		function(Request $request, $course_id, $id) use($app) {

	$name = $request->request->get('name');
	$learning_outcomes = $request->request->get('learning_outcomes');
	$details = $request->request->get('details');
	$type = $request->request->get("type");
	$week = $request->request->get("week");
	$weight = $request->request->get("weight");

	if(!empty($learning_outcomes)) {
		if(!is_array($learning_outcomes)) {
			$learning_outcomes = json_decode($learning_outcomes);
			//return buildResponse(json_encode(array("error" => "Learning outcomes must be an array.")), Response::HTTP_BAD_REQUEST);
		}

		$learning_outcomes = json_encode($learning_outcomes);
	} else {
		$learning_outcomes = json_encode(array());
	}

	if(!empty($week) && $week !== "ongoing" && !is_numeric($week)) {

		if(strpos($week, "_") !== FALSE) {
			$a = explode("_", $week);

			if(!is_numeric($a[0]) || !is_numeric($a[1])) {
				return buildResponse(json_encode(array("error" => "Week must be an integer, underscore separated range e.g. '2_3' or 'ongoing'")), Response::HTTP_BAD_REQUEST);
			}
		} else {
			return buildResponse(json_encode(array("error" => "Week must be an integer, underscore separated range e.g. '2_3' or 'ongoing'")), Response::HTTP_BAD_REQUEST);
		}
	}

	if(!is_numeric($weight)) {
		return buildResponse(json_encode(array("error" => "Weight must be single decimal place floating number (e.g. 0.1 for 10%)")), Response::HTTP_BAD_REQUEST);
	}

	if(!is_numeric($id)) {
		return buildResponse(json_encode(array("error" => "Bad assessment id")), Response::HTTP_BAD_REQUEST);
	}

	$app['db']->update("assessments", array('name' => $name, 'details' => $details, 'type' => $type, 'learning_outcomes' => $learning_outcomes,
			'week' => $week, 'weight' => $weight, 'course_id' => $course_id),  array('id' => $id));

	$row = array('name' => $name, 'details' => $details, 'type' => $type, 'learning_outcomes' => $learning_outcomes,
			'week' => $week, 'weight' => $weight, 'learning_outcome_id' => $course_id);

	$response = buildResponse(json_encode($row), Response::HTTP_OK);

	return $response;
});
/*
 * Delete single assessment
 */
$app->delete('/courses/{course_id}/assessments/{id}',
		function(Request $request, $course_id, $id) use($app) {

			if(!is_numeric($id)) {
				return buildResponse(json_encode(array("error" => "Bad assessment id")), Response::HTTP_BAD_REQUEST);
			}

			$app['db']->delete("assessments", array('id' => $id, 'course_id' => $course_id));
			$row = array("id" => $id, 'course_id' => $course_id);

			$response = buildResponse(json_encode($row), Response::HTTP_OK);

			return $response;
});

/*
 * Criteria (for Rubric rows)
 * -----------
 */
/*
 * Create a new criterion using POST request
 *
 * This is used to populate the rubric rows.
 */
$app->post('/assessments/{assessment_id}/criteria', function(Request $request, $assessment_id) use($app) {

$description = $request->request->get('description');
$col1 = $request->request->get('col1');
$col2 = $request->request->get("col2");
$col3 = $request->request->get("col3");
$col4 = $request->request->get("col4");
$col5 = $request->request->get("col5");


if(!is_numeric($assessment_id)) {
	return buildResponse(json_encode(array("error" => "Bad assessment id")), Response::HTTP_BAD_REQUEST);
}

$app['db']->insert("criteria", array('assessment_id' => $assessment_id, 'description' => $description, 'col1' => $col1, 'col2' => $col2, 'col3' => $col3, 'col4' => $col4, 'col5' => $col5));
$id =  $app['db']->lastInsertId();

$row = array("id" => $id, 'assessment_id' => $assessment_id, 'description' => $description, 'col1' => $col1, 'col2' => $col2, 'col3' => $col3, 'col4' => $col4, 'col5' => $col5);

$location = "/assessments/$assessment_id/criteria/$id";

$response = buildResponse(json_encode($row), Response::HTTP_CREATED, $location);

	return $response;
});

/*
 * Get criteria for assessment
*/
$app->get('/assessments/{assessment_id}/criteria', function(Request $request, $assessment_id) use($app) {
	$page = $request->get("page");
	$limit = $request->get("limit");

	if(empty($assessment_id) || !is_numeric($assessment_id)) {
		return buildResponse(json_encode(array("error" => "Bad learning outcome id")), Response::HTTP_BAD_REQUEST);
	}

	$offset = ($page - 1) * $limit;

	$sql = sprintf("SELECT * FROM criteria WHERE assessment_id = '%d' LIMIT %d, %d", $assessment_id, $offset, $limit);
	$post = $app['db']->fetchAll($sql);
	$response = buildResponse(json_encode($post), Response::HTTP_OK);

	return $response;
});
/*
 * Get single criteria
	*/
$app->get('/assessments/{assessment_id}/criteria/{id}', function(Request $request, $assessment_id, $id) use($app) {

	if(empty($id) || !is_numeric($id)) {
		return buildResponse(json_encode(array("error" => "Bad assessment id")), Response::HTTP_BAD_REQUEST);
	}

	$sql = sprintf("SELECT * FROM criteria WHERE id = '%d'", $id);
	$post = $app['db']->fetchAll($sql);
	$response = buildResponse(json_encode($post[0]), Response::HTTP_OK);

	return $response;
});

/*
 * Put single criteria
*/
$app->put('/assessments/{assessment_id}/criteria/{id}',
		function(Request $request, $assessment_id, $id) use($app) {

	$description = $request->request->get('description');
	$col1 = $request->request->get('col1');
	$col2 = $request->request->get("col2");
	$col3 = $request->request->get("col3");
	$col4 = $request->request->get("col4");
	$col5 = $request->request->get("col5");

	if(!is_numeric($id)) {
		return buildResponse(json_encode(array("error" => "Bad learning outcome id")), Response::HTTP_BAD_REQUEST);
	}

	$app['db']->update("criteria", array('assessment_id' => $assessment_id, 'description' => $description, 'col1' => $col1, 'col2' => $col2, 'col3' => $col3, 'col4' => $col4, 'col5' => $col5),  array('id' => $id));

	$row = array('assessment_id' => $assessment_id, 'description' => $description, 'col1' => $col1, 'col2' => $col2, 'col3' => $col3, 'col4' => $col4, 'col5' => $col5);

	$response = buildResponse(json_encode($row), Response::HTTP_OK);

return $response;
});
/*
* Delete single critera
*/
$app->delete('/assessments/{assessment_id}/criteria/{id}',
		function(Request $request, $course_id, $assessment_id, $id) use($app) {

	if(!is_numeric($id)) {
		return buildResponse(json_encode(array("error" => "Bad criteria id")), Response::HTTP_BAD_REQUEST);
	}

	$app['db']->delete("criteria", array('id' => $id, 'assessment_id' => $assessment_id));
	$row = array("id" => $id, 'assessment_id' => $assessment_id);

	$response = buildResponse(json_encode($row), Response::HTTP_OK);

	return $response;
});

/*
 * Summary for course
 */
$app->get('/course_summary/{course_id}', function(Request $request, $course_id) use($app) {
	$page = $request->get("page");
	$limit = $request->get("limit");

	if(empty($course_id) || !is_numeric($course_id)) {
		return buildResponse(json_encode(array("error" => "Bad course id - must be numeric")), Response::HTTP_BAD_REQUEST);
	}

	$offset = ($page - 1) * $limit;

	$sql = sprintf("SELECT
					`learning_outcomes`.`id` as `learning_outcome_id`,
					`learning_outcomes`.`name` as `learning_outcome_name`,
					`learning_activities`.`name` as `learning_activity_name`,
					`assessments`.`name` as `assessment_name`, `learning_activities`.`learning_outcomes` as `learning_activity_outcomes`,
					`assessments`.`learning_outcomes` as `assessment_outcomes`
					FROM `learning_outcomes`
					LEFT JOIN `learning_activities` ON `learning_outcomes`.`course_id` = `learning_activities`.`course_id`
					LEFT JOIN `assessments` ON `learning_outcomes`.`course_id` = `assessments`.`course_id`
					WHERE`learning_outcomes`.`course_id` = '%d' LIMIT %d, %d",
					$course_id, $offset, $limit);

	$post = $app['db']->fetchAll($sql);
	$response = buildResponse(json_encode($post), Response::HTTP_OK);

	return $response;
});

function buildResponse($content, $success_code, $location = NULL) {

	if($location === NULL) $location = $_SERVER['REQUEST_URI'];

	$response = new Response();

	$response->setContent($content);
	$response->setStatusCode($success_code);
	$response->headers->set('Location', $location);
	$response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE');
    $response->headers->set('Access-Control-Allow-Origin', '*');
	$response->headers->set('Content-Type', 'application/json');

	return $response;
}

/**
    Handle OPTIONS request as documented here:
    http://stackoverflow.com/questions/19409105/cors-preflight-request-returning-http-405/19413480#19413480
*/
$app->match("{url}", function($url) use ($app) { return "OK"; })->assert('url', '.*')->method("OPTIONS");

$app->run();
?>
