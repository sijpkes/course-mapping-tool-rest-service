    	<?php

/**
    This file initializes the index.html with Backbone.js

    This is more efficient than building the views via Ajax
    (requires a single call to the server rather than two calls).
*/
    			$course_coll = array();

    			$servername = "localhost";
    			$username = "root";
    			$password = "u0nb0ld";
    			$db_name = "course_mapper";

    			// Create connection
    			$conn = new mysqli($servername, $username, $password, $db_name);
    			// Check connection
    			if ($conn->connect_error) {
    			    die("Connection failed: " . $conn->connect_error);
    			}

    			$sql = "SELECT * FROM courses LIMIT 0,100";
    			$result = $conn->query($sql);

    			if ($result->num_rows > 0) {
    				while($row = $result->fetch_assoc()) {
    			   		$course_coll[] = $row;
    				}
    			}
    			$conn->close();

    			$courses = json_encode($course_coll);
    	?>

    var courseListCollection = new app.courses(<?= $courses ?>);
	var homeListView = new app.courseListView({ collection: courseListCollection });

	var header = "<thead>\
					  <tr>\
					    <th>Course Name</th>\
					    <th>Course Code</th>\
					    <th>Course Duration (wks)</th>\
					  </tr>\
				</thead>";

	$("#courses").html(header).append(homeListView.render().el);
